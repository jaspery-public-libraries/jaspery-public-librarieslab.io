/*
 * Copyright © 2021. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

plugins {
    val versions = object {
        val orchid = "0.21.0"
    }
    id("com.eden.orchidPlugin") version versions.orchid
}

object Versions {
    const val kotlin = "1.4.31"
    const val orchid = "0.21.0"
}

@Suppress("UnstableApiUsage")
repositories {
    jcenter {
        mavenContent {
            includeGroup("com.github.JavaEden")
            includeGroup("io.github.javaeden.orchid")
            includeGroup("com.github.copper-leaf.dokka-json")
            includeGroup("com.eden")
            includeGroup("com.eden.kodiak")
        }
    }
    maven(url = "https://jitpack.io") {
        mavenContent {
            includeGroup("com.gitlab.jaspery-public-libraries.jaspery-kotlin-ext")
        }
    }
    maven(url = "https://kotlin.bintray.com/kotlinx") {
        mavenContent {
            includeGroup("org.jetbrains.kotlinx")
        }
    }
    mavenCentral {
        mavenContent {
            excludeGroup("com.gitlab.jaspery-public-libraries.jaspery-kotlin-ext")
        }
    }
}

val docFiles by configurations.creating {
    isTransitive = false
}

val orchidSrc4KDoc by configurations.creating {
    isTransitive = false
}

val jkeGroup = "com.gitlab.jaspery-public-libraries.jaspery-kotlin-ext"
val jkeVersion = "master-SNAPSHOT"

dependencies {
    orchidImplementation(kotlin("stdlib-jdk8", Versions.kotlin))
    orchidImplementation("io.github.javaeden.orchid:OrchidAll:${Versions.orchid}") {
        exclude("junit")
    }
    orchidRuntimeOnly("io.github.javaeden.orchid:OrchidBsDoc:${Versions.orchid}") {
        exclude("junit")
    }
    orchidRuntimeOnly("io.github.javaeden.orchid:OrchidKotlindoc:${Versions.orchid}") {
        exclude("junit")
    }
    orchidSrc4KDoc("$jkeGroup:jaspery-kotlin-lang:$jkeVersion:sources@jar")
    docFiles("$jkeGroup:jaspery-kotlin-ext:$jkeVersion:site@zip")

    @Suppress("UnstableApiUsage")
    constraints {
        orchidImplementation("com.google.code.gson:gson") {
            version {
                require("2.8.6")
                because("Orchid 0.21.0 uses gson 2.8.1 but across the board we bump to higher versions")
            }
        }
        orchidImplementation(kotlin("reflect")) {
            version {
                require(Versions.kotlin)
                because("Orchid 0.21.0 uses 1.3.50 but across the board we bump to higher versions")
            }
        }
        orchidImplementation("com.beust:jcommander") {
            version {
                require("1.78")
                because("Orchid 0.21.0 uses 1.72 but across the board we bump to higher versions")
            }
        }
    }
}

val SITE_BASE_URL_PROPERTY = "site.baseUrl"
val SITE_BASE_URL_DEFAULT = "http://localhost:8080"

val orchidSourceSet by sourceSets.named("orchid")

orchid {
    theme = "BsDoc"
    baseUrl = findProperty(SITE_BASE_URL_PROPERTY) as? String ?: SITE_BASE_URL_DEFAULT
    srcDir = orchidSourceSet.output.resourcesDir?.toRelativeString(project.rootDir)
}

val unpackDocResources = tasks.register<Copy>("unpackDocResources") {
    docFiles.dependencies.forEach {
        from(zipTree(docFiles.fileCollection(it).singleFile)) {
            into(it.name)
        }
    }

    into("$buildDir/tmp/docResources")
}

val unpackSources4KDoc = tasks.register<Copy>("unpackSources4KDoc") {
    orchidSrc4KDoc.dependencies.forEach {
        from(zipTree(orchidSrc4KDoc.fileCollection(it).singleFile)) {
            into(it.name)
        }
    }

    into("$buildDir/tmp/sources4KDoc")
}

tasks.named<ProcessResources>("processOrchidResources") {
    dependsOn(unpackDocResources, unpackSources4KDoc)
    from(unpackDocResources.get().destinationDir) {
        rename("README\\.md", "index.md")
        into("pages/prj")
    }
}
